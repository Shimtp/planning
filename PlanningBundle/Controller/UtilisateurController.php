<?php

namespace Iut\PlanningBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class UtilisateurController extends Controller
{
    /**
     * @Route("/add:")
     * @Template()
     */
    public function add:Action()
    {
    }

    /**
     * @Route("/show:")
     * @Template()
     */
    public function show:Action()
    {
    }

}
