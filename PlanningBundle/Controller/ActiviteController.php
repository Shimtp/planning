<?php

namespace Iut\PlanningBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ActiviteController extends Controller
{
    /**
     * @Route("/saisir:")
     * @Template()
     */
    public function saisir:Action()
    {
    }

    /**
     * @Route("/showAct:")
     * @Template()
     */
    public function showAct:Action()
    {
    }

    /**
     * @Route("/showCre:")
     * @Template()
     */
    public function showCre:Action()
    {
    }

    /**
     * @Route("/supr:")
     * @Template()
     */
    public function supr:Action()
    {
    }

    /**
     * @Route("/modif:")
     * @Template()
     */
    public function modif:Action()
    {
    }

}
