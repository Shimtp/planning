<?php

namespace Iut\PlanningBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Activite
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Activite
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NomAct", type="string", length=50)
     */
    private $nomAct;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomAct
     *
     * @param string $nomAct
     * @return Activite
     */
    public function setNomAct($nomAct)
    {
        $this->nomAct = $nomAct;

        return $this;
    }

    /**
     * Get nomAct
     *
     * @return string 
     */
    public function getNomAct()
    {
        return $this->nomAct;
    }
}
