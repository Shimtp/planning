<?php

namespace Iut\PlanningBundle\Entity\src;

use Doctrine\ORM\Mapping as ORM;

/**
 * Iut
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Iut
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
