<?php

namespace Iut\PlanningBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Utilisateur
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Utilisateur
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NomUt", type="string", length=20)
     */
    private $nomUt;

    /**
     * @var string
     *
     * @ORM\Column(name="PrenomUt", type="string", length=20)
     */
    private $prenomUt;

    /**
     * @var string
     *
     * @ORM\Column(name="MailUt", type="string", length=50)
     */
    private $mailUt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomUt
     *
     * @param string $nomUt
     * @return Utilisateur
     */
    public function setNomUt($nomUt)
    {
        $this->nomUt = $nomUt;

        return $this;
    }

    /**
     * Get nomUt
     *
     * @return string 
     */
    public function getNomUt()
    {
        return $this->nomUt;
    }

    /**
     * Set prenomUt
     *
     * @param string $prenomUt
     * @return Utilisateur
     */
    public function setPrenomUt($prenomUt)
    {
        $this->prenomUt = $prenomUt;

        return $this;
    }

    /**
     * Get prenomUt
     *
     * @return string 
     */
    public function getPrenomUt()
    {
        return $this->prenomUt;
    }

    /**
     * Set mailUt
     *
     * @param string $mailUt
     * @return Utilisateur
     */
    public function setMailUt($mailUt)
    {
        $this->mailUt = $mailUt;

        return $this;
    }

    /**
     * Get mailUt
     *
     * @return string 
     */
    public function getMailUt()
    {
        return $this->mailUt;
    }
}
