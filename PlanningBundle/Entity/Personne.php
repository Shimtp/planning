<?php

namespace Iut\PlanningBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Personne
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Personne
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NomPers", type="string", length=20)
     */
    private $nomPers;

    /**
     * @var string
     *
     * @ORM\Column(name="PrenomPers", type="string", length=20)
     */
    private $prenomPers;

    /**
     * @var string
     *
     * @ORM\Column(name="MailPers", type="string", length=100)
     */
    private $mailPers;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomPers
     *
     * @param string $nomPers
     * @return Personne
     */
    public function setNomPers($nomPers)
    {
        $this->nomPers = $nomPers;

        return $this;
    }

    /**
     * Get nomPers
     *
     * @return string 
     */
    public function getNomPers()
    {
        return $this->nomPers;
    }

    /**
     * Set prenomPers
     *
     * @param string $prenomPers
     * @return Personne
     */
    public function setPrenomPers($prenomPers)
    {
        $this->prenomPers = $prenomPers;

        return $this;
    }

    /**
     * Get prenomPers
     *
     * @return string 
     */
    public function getPrenomPers()
    {
        return $this->prenomPers;
    }

    /**
     * Set mailPers
     *
     * @param string $mailPers
     * @return Personne
     */
    public function setMailPers($mailPers)
    {
        $this->mailPers = $mailPers;

        return $this;
    }

    /**
     * Get mailPers
     *
     * @return string 
     */
    public function getMailPers()
    {
        return $this->mailPers;
    }
}
